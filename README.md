# LORASC
Robust Algebraic Schur Complement Preconditioners Based on Low Rank Corrections

## Programming

### Git
* Uses [Bitbucket](https://bitbucket.org/emfomy/lorasc) to host.
* Uses [git-flow](http://nvie.com/posts/a-successful-git-branching-model/) to control branches.

### C++
* Uses [Google C++ Style Guide](http://google-styleguide.googlecode.com/svn/trunk/cppguide.html) but uses '.cpp' and '.hpp' as extensions.

### Packages
* [SuiteSparse 4.4.4](http://faculty.cse.tamu.edu/davis/suitesparse.html)
* [METIS 4.0.1](http://glaros.dtc.umn.edu/gkhome/views/metis)
* [Intel® Math Kernel Library 11.1](https://software.intel.com/en-us/mkl_11.1_ref)
* [CUDA Toolkit 6.5](https://developer.nvidia.com/cuda-toolkit-65)

## Algorithm

### Createing LORASC Preconditioner

#### Reordering
Reorder the **symmetric positive definite** matrix to
```
#!text
    [                    ]
    [ A_11          A_11 ]
A = [       %        %   ]
    [          A_nn A_ns ]
    [ A_s1  %  A_sn A_ss ]
    [                    ]
```

#### Factorization
The LDU-factorization is
```
#!text
    [                    ] [                                ] [                    ]
    [ A_11               ] [ inv(A_11)                      ] [ A_11          A_1s ]
A = [       %            ] [            %                   ] [       %        %   ]
    [          A_nn      ] [               inv(A_nn)        ] [          A_nn A_ns ]
    [ A_s1  %  A_sn  S   ] [                         inv(S) ] [                S   ]
    [                    ] [                                ] [                    ]

    [                                      ] [                  ] [                         ]
    [       I                              ] [ A_11             ] [ I        inv(A_11)*A_1s ]
  = [                 %                    ] [       %          ] [    %            %       ]
    [                          I           ] [          A_nn    ] [       I  inv(A_ss)*A_ns ]
    [ A_s1*inv(A_11)  %  A_sn*inv(A_nn)  I ] [                S ] [                 I       ]
    [                                      ] [                  ] [                         ]
```
where *S = A_ss - sum(A_si inv(A_ii) A_is)* is the Schur complement.

#### Generalized Eigenvalues Problem
Let *λ_i* be the smallest *l* generalized eigenvalues of *S u = λ A_ss u* and *v_i* be the corresponding generalized eigenvectors. Define *σ_i = (ε-λ_i)/λ_i*, where *ε = max(λ_i)*. Note that *ε ≤ κ(inv(P)S) ≤ 1*.

#### Approximate Inverse of the Schur Complement
The approximate inverse of the Schur complement is *inv(P) = inv(A_ss) + VDV'*, where *V = [v_1, ..., V_l]* and *D = diag(σ_1, ..., σ_l)*.

### Randomized Algorithm
0. Input: a *m×m* symmetric positive definite matrix *B*, and the desired rank *l*.
1. Sample an *m×l* test matrix *Q* with independent mean-zero, unit-variance Gaussian entries.
2. Let *H ← BQ*.
3. Compute the QR-decomposition *H = QR*.
4. Repeat 2-3 for some iterations.
5. Let *C = BQ*.
6. Compute the SVD-decomposition *C = VDU'*
7. Output: the largest eigenvalues *D*, and the corresponding eigenvectors *V*.

#### Trick
Rewrite generalized eigenvalues problem as *A_ss u - S u = A_ss u - λ A_ss u*, which is equivalent to *inv(A_ss)(A_ss - S)u = ζu*, where *ζ = 1 - λ*. Since *0 ≤ λ < 1*, we have *0 < ζ ≤ 1* and the smallest eigenvalues *λ_l* correspond to the largest eigenvalues *ζ_l*.

Write *A_ss = LL'* be the Cholresky decomposition, then *inv(L)(A_ss - S)inv(L')w = ζw*, where *w = L'u*. Since *inv(L)(A_ss - S)inv(L')* is symmetric positive definite, we may use iterative solver (Randomized Algorithm) to solve the generalized eigenvalues problem.

### LORASC Perconditioner
The LORASC preconditioner is
```
#!text
    [                                      ] [                  ] [                         ]
    [       I                              ] [ A_11             ] [ I        inv(A_11)*A_1s ]
M = [                 %                    ] [       %          ] [    %            %       ]
    [                          I           ] [          A_nn    ] [       I  inv(A_ss)*A_ns ]
    [ A_s1*inv(A_11)  %  A_sn*inv(A_nn)  I ] [                P ] [                 I       ]
    [                                      ] [                  ] [                         ]

      [                  ]        [                         ]
      [ A_11             ]        [ I        inv(A_11)*A_1s ]
M_L = [       %          ]  M_U = [    %            %       ]
      [          A_nn    ]        [       I  inv(A_ss)*A_ns ]
      [ A_s1  %  A_sn  P ]        [                 I       ]
      [                  ]        [                         ]
```
The inverse is
```
#!text
         [                          ] [                                 ] [                                        ]
         [ I        -inv(A_11)*A_1s ] [ inv(A_11)                       ] [        I                               ]
inv(M) = [    %            %        ] [            %                    ] [                  %                     ]
         [       I  -inv(A_ss)*A_ns ] [               inv(A_nn)         ] [                            I           ]
         [                 I        ] [                          inv(P) ] [ -A_s1*inv(A_11)  %  -A_sn*inv(A_nn)  I ]
         [                          ] [                                 ] [                                        ]
```
The pruduct is
```
#!text
           [                   ]
           [ I           X_1   ]
inv(M)*A = [    %         %    ]
           [       I     X_N   ]
           [          inv(P)*S ]
           [                   ]
```

### Solving Linear System
Solve *Ax = b*. Rewrite the system as *inv(M) A x = inv(M) b*.

#### Applying Lower LORASC Perconditioner
Solve *M_L y = b*:
```
#!text
[                  ] [     ]   [     ]
[ A_11             ] [ y_1 ]   [ b_1 ]
[       %          ] [  %  ] = [  %  ]
[          A_nn    ] [ y_n ]   [ b_n ]
[ A_s1  %  A_sn  P ] [ y_n ]   [ b_n ]
[                  ] [     ]   [     ]
```
1. Solve *y_i* from *A_ii y_i = b_i*.
2. Compute *z_s = b_s - sum(A_si y_i)*.
3. Compute *y_s = P z_s*.

#### Applying Upper LORASC Perconditioner
Solve *M_U d = y*:
```
#!text
[                         ] [     ] = [     ]
[ I        inv(A_11)*A_1s ] [ d_1 ] = [ y_1 ]
[    %            %       ] [  %  ] = [  %  ]
[       I  inv(A_ss)*A_ns ] [ d_n ] = [ y_n ]
[                 I       ] [ d_n ] = [ y_n ]
[                         ] [     ] = [     ]
```
1. *d_i = y_i*.
2. Solve *c_i* from *A_ii c_i = A_is y_s*.
3. Compute *d_i = y_i - c_i*.

#### Applying Upper LORASC Perconditioner
Solve *inv(M)Ax = d*:
```
#!text
[                   ] [     ] = [     ]
[ I           X_1   ] [ x_1 ] = [ d_1 ]
[    %         %    ] [  %  ] = [  %  ]
[       I     X_N   ] [ x_n ] = [ d_n ]
[          inv(P)*S ] [ x_n ] = [ d_n ]
[                   ] [     ] = [     ]
```
1. Solve *x_s* from *S x_s = z_s* by PCG using *inv(P)* as preconditioner.
2. Solve *e_i* from *A_ii e_i = A_is (d_s - x_s)*.
3. Compute *x_i = d_i - e_i*.

## Reference
* [Grigori, L., Nataf, F., & Yousef, S. (2014, July 1). Robust Algebraic Schur Complement Preconditioners Based on Low Rank Corrections. Retrieved April 7, 2015](https://hal.inria.fr/hal-01017448/document)
* [Witten, R., & Candès, E. (2014). Randomized Algorithms for Low-Rank Matrix Factorizations: Sharp Performance Bounds. Algorithmica, 72(1), 264–281.](http://doi.org/10.1007/s00453-014-9891-7)
