# Robust Algebraic Schur Complement Preconditioners Based on Low Rank Corrections
# The main Makefile

include Makefile.inc

SUBDIR = src demo

.PHONY: all $(SUBDIR) clean

all: $(SUBDIR)
	@echo > /dev/null

$(SUBDIR):
	@( cd $@ ; $(MAKE) all )

clean:
	@for dir in $(SUBDIR); do ( cd $$dir ; $(MAKE) clean ) done
