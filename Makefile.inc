# Robust Algebraic Schur Complement Preconditioners Based on Low Rank Corrections
#
# Make sure you have environement variables
# 
# source /opt/intel/composer_xe_2013_sp1.0.080/bin/compilervars.sh intel64 mod
# source /opt/intel/composer_xe_2013_sp1.0.080/mkl/bin/mklvars.sh intel64 mod
#
# PATH=$PATH:/opt/openmpi-intel-2013-1.7.2/bin
# PATH=$PATH:/opt/NVIDIA/cuda-6.5/bin
# export PATH
# LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/NVIDIA/cuda-6.5/bin/lib64
# export LD_LIBRARY_PATH
#
# SuiteSparse should be put in $(HOME)/opt/
#

SUITESPATSE = $(HOME)/opt/SuiteSparse

MAKEFLAGS += --no-print-directory

include $(SUITESPATSE)/SuiteSparse_config/SuiteSparse_config.mk

SUITESPARSE_CONFIG = $(SUITESPATSE)/SuiteSparse_config

CHOLMOD = $(SUITESPATSE)/CHOLMOD
AMD     = $(SUITESPATSE)/AMD
CAMD    = $(SUITESPATSE)/CAMD
COLAMD  = $(SUITESPATSE)/COLAMD
CCOLAMD = $(SUITESPATSE)/CCOLAMD
METIS   = $(SUITESPATSE)/metis-4.0

INCLUDE = \
	-I$(CHOLMOD)/Include \
	-I$(SUITESPARSE_CONFIG) \

LIBRARY = \
	$(CHOLMOD)/Lib/libcholmod.a \
	$(METIS)/libmetis.a \
	$(AMD)/Lib/libamd.a \
	$(CAMD)/Lib/libcamd.a \
	$(COLAMD)/Lib/libcolamd.a \
	$(CCOLAMD)/Lib/libccolamd.a \
	$(SUITESPARSE_CONFIG)/libsuitesparseconfig.a \
	$(CUDART_LIB) $(CUBLAS_LIB) \
	$(BLAS) -lrt \

CXXFLAGS = $(CF) -std=c++11 $(CHOLMOD_CONFIG)
