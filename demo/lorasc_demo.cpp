////////////////////////////////////////////////////////////////////////////////
// Robust Algebraic Schur Complement Preconditioners                          //
//     Based on Low Rank Corrections                                          //
//                                                                            //
// lorasc_demo.cpp                                                            //
// The demo function                                                          //
//                                                                            //
// Author: emfo<emfomy@gmail.com>                                             //
////////////////////////////////////////////////////////////////////////////////

#include <ctime>
#include <iostream>
#include "lorasc.hpp"
#include "mkl.h"
using namespace std;
using namespace lorasc;

////////////////////////////////////////////////////////////////////////////////
// Main function                                                              //
////////////////////////////////////////////////////////////////////////////////
int main( int argc, char *argv[] ) {
  double done[] = {1.0, 0.0}, dnone[] = {-1.0, 0.0};
  clock_t t;

  // Open file
  FILE *file;
  file = fopen(argv[1], "r");
  if ( !file ) {
    printf("Unable to open file '%s'!\n", argv[1]);
    exit(1);
  }

  // Start CHOLMOD
  cholmod_common common;
  cholmod_start(&common);
  cout << "================================================================\n";

  // Read matrix from file
  auto mat = cholmod_read_sparse(file, &common);
  auto rhs = cholmod_ones(mat->nrow, 2, CHOLMOD_REAL, &common);
  cholmod_print_sparse(mat, "mat", &common);
  cholmod_print_dense(rhs, "rhs", &common);
  cout << "================================================================\n";

  // Solve directly
  cout << "Solving directly..." << flush;
  t = clock();
  {
    auto fac = cholmod_analyze(mat, &common);
    cholmod_factorize(mat, fac, &common);
    auto sol = cholmod_solve(CHOLMOD_A, fac, rhs, &common);
    cholmod_free_factor(&fac, &common);
    cholmod_free_dense(&sol, &common);
  }
  cout << "\rSystem solved directly......... "
       << static_cast<double>(clock()-t) / CLOCKS_PER_SEC
       << " seconds." << endl;
  cout << "================================================================\n";

  // Create LORASC solver
  const size_t num_level = (argc > 2) ? atoi(argv[2]) : 3;
  Lorasc lorasc(num_level, &common);
  cout << "Set Lorasc with " << (1 << num_level) << " domains." << endl;

  // Put the matrix into LORASC solver
  cout << "Creating Lorasc..." << flush;
  t = clock();
  lorasc.Read(mat);
  cout << "\rLorasc created ................ "
       << static_cast<double>(clock()-t) / CLOCKS_PER_SEC
       << " seconds." << endl;

  // Solve the system
  cout << "Solving linear system..." << flush;
  t = clock();
  auto sol = cholmod_copy_dense(rhs, &common);
  lorasc.Solve(sol);
  cout << "\rSystem solved ................. "
       << static_cast<double>(clock()-t) / CLOCKS_PER_SEC
       << " seconds." << endl;
  cout << "================================================================\n";

  // Compute relative esidual
  auto res = cholmod_copy_dense(rhs, &common);
  cholmod_sdmult(mat, false, dnone, done, sol, res, &common);
  auto err = cblas_dnrm2(res->nzmax, reinterpret_cast<double*>(res->x), 1)
           / cblas_dnrm2(rhs->nzmax, reinterpret_cast<double*>(rhs->x), 1);
  cholmod_print_dense(sol, "sol", &common);
  cholmod_print_dense(res, "res", &common);
  cout << "Relative residual is " << err << "." << endl;
  cout << "================================================================";

  // Disply GPU statistics
  cholmod_gpu_stats(&common);
  cout << "================================================================\n";

  // Delete matrices
  cholmod_free_sparse(&mat, &common);
  cholmod_free_dense(&rhs, &common);
  cholmod_free_dense(&sol, &common);
  cholmod_free_dense(&res, &common);

  // Finish CHOLMOD
  cholmod_finish(&common);

  return 0;
}
