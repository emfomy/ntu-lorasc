////////////////////////////////////////////////////////////////////////////////
// Robust Algebraic Schur Complement Preconditioners                          //
//     Based on Low Rank Corrections                                          //
//                                                                            //
// lorasc_domain.hpp                                                          //
// The header file of the struct LorascDomain                                 //
//                                                                            //
// Author: emfo<emfomy@gmail.com>                                             //
////////////////////////////////////////////////////////////////////////////////

#ifndef LORASC_LORASC_DOMAIN_HPP_

#define LORASC_LORASC_DOMAIN_HPP_

#include "cholmod.h"
#include "lorasc_const.hpp"
#include "lorasc_solver.hpp"

////////////////////////////////////////////////////////////////////////////////
// The namespace lorasc                                                       //
////////////////////////////////////////////////////////////////////////////////
namespace lorasc {

////////////////////////////////////////////////////////////////////////////////
// The struct of a LORASC domain                                              //
////////////////////////////////////////////////////////////////////////////////
struct LorascSolver::LorascDomain {
  // the interior matrix (A_ii)
  // sparse symmmetric positive definite
  cholmod_sparse* matrix_;

  // the factor of the interior matrix (L_ii)
  // sparse lower triangular
  cholmod_factor* factor_;

  // the border matrix (A_is)
  // sparse rectangle
  cholmod_sparse* border_;

  // the interior right-hand-side vectors
  cholmod_dense* vec_b_;
  cholmod_dense* vec_y_;
  cholmod_dense* vec_c_;
  cholmod_dense* vec_d_;
  cholmod_dense* vec_x_;

  // the number of rows of the interior matrix
  size_t nrow_;

  // the permutation array
  int* perm_;

  // the CHOLMOD common object
  cholmod_common* common_;

 public:
  // the mauual constructor
  void Construct( cholmod_common* common );

  // the mauual destructor
  void Destruct();

  // bisect this domain
  void Bisect( int inc,
               int* perm_s,
               size_t& size_s );
};

}

#endif
