////////////////////////////////////////////////////////////////////////////////
// Robust Algebraic Schur Complement Preconditioners                          //
//     Based on Low Rank Corrections                                          //
//                                                                            //
// lorasc_const.hpp                                                           //
// The header file of the constants                                           //
//                                                                            //
// Author: emfo<emfomy@gmail.com>                                             //
////////////////////////////////////////////////////////////////////////////////

#ifndef LORASC_LORASC_CONST_HPP_

#define LORASC_LORASC_CONST_HPP_

#include <ctime>

////////////////////////////////////////////////////////////////////////////////
// The namespace lorasc                                                       //
////////////////////////////////////////////////////////////////////////////////
namespace lorasc {

// Constants
static double done[]  = { 1.0, 0.0};
static double dnone[] = {-1.0, 0.0};
static double dzero[] = { 0.0, 0.0};
static int iseed[] = {static_cast<int>(time(NULL)%4096),
                      static_cast<int>(time(NULL)%4096),
                      static_cast<int>(time(NULL)%4096), 1};

}

#endif
