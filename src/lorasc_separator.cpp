////////////////////////////////////////////////////////////////////////////////
// Robust Algebraic Schur Complement Preconditioners                          //
//     Based on Low Rank Corrections                                          //
//                                                                            //
// lorasc_separator.cpp                                                       //
// The struct LorascSeparator                                                 //
//                                                                            //
// Author: emfo<emfomy@gmail.com>                                             //
////////////////////////////////////////////////////////////////////////////////

#include "lorasc_separator.hpp"
#include "mkl.h"

////////////////////////////////////////////////////////////////////////////////
// The namespace lorasc                                                       //
////////////////////////////////////////////////////////////////////////////////
namespace lorasc {

////////////////////////////////////////////////////////////////////////////////
// The manual constructor of LorascSeparator                                  //
//                                                                            //
// Parameters:                                                                //
// common: the CHOLMOD common object                                          //
////////////////////////////////////////////////////////////////////////////////
void LorascSolver::LorascSeparator::Construct( cholmod_common* common ) {
  common_ = common;
  matrix_ = nullptr;
  factor_ = nullptr;
  schur2_ = nullptr;
  eigvec_ = nullptr;
  eigval_ = nullptr;
  schur_func_       = nullptr;
  schur2_conj_func_ = nullptr;
  schur_prec_func_  = nullptr;
  perm_   = nullptr;
}

////////////////////////////////////////////////////////////////////////////////
// The manual destructor of LorascSeparator                                   //
////////////////////////////////////////////////////////////////////////////////
void LorascSolver::LorascSeparator::Destruct() {
  cholmod_free_sparse(&matrix_, common_);
  cholmod_free_factor(&factor_, common_);
  cholmod_free_sparse(&schur2_, common_);
  cholmod_free_dense(&eigvec_, common_);
  cholmod_free_sparse(&eigval_, common_);
  delete schur_func_;
  delete schur2_conj_func_;
  delete schur_prec_func_;
  delete perm_;
}

////////////////////////////////////////////////////////////////////////////////
// The constructor of SchurFunc                                               //
//                                                                            //
// Parameters:                                                                //
// factor: the separator matrix                                               //
// schur2: the second term of the Schur complement                            //
// common: the CHOLMOD common object                                          //
////////////////////////////////////////////////////////////////////////////////
LorascSolver::LorascSeparator::SchurFunc::SchurFunc(
    cholmod_sparse* matrix, cholmod_sparse* schur2, cholmod_common* common ) {
  common_ = common;
  schur_  = cholmod_add(
      matrix,  // [in]  augend
      schur2,  // [in]  addend
      done,    // [in]  scalar of augend
      dnone,   // [in]  scalar of addend
      true,    // [in]  compute numerical values
      false,   // [in]  not sort sum columns
      common_  //       CHOLMOD common object
  );
  nrow_   = schur_->nrow;
}

////////////////////////////////////////////////////////////////////////////////
// The destructor of SchurFunc                                                //
////////////////////////////////////////////////////////////////////////////////
LorascSolver::LorascSeparator::SchurFunc::~SchurFunc() {
  cholmod_free_sparse(&schur_, common_);
}

////////////////////////////////////////////////////////////////////////////////
// Apply the Schur2 conjugate                                                 //
//                                                                            //
// Parameters:                                                                //
// src: the source matrix                                                     //
//                                                                            //
// Return Value:                                                              //
// the destination matrix, equal to                                           //
//   schur_ * src                                                             //
////////////////////////////////////////////////////////////////////////////////
cholmod_dense* LorascSolver::LorascSeparator::SchurFunc::operator()(
    cholmod_dense* src ) {
  // dst := schur_ * src
  auto dst = cholmod_allocate_dense(
      nrow_,       // [in]  #row
      src->ncol,   // [in]  #col
      nrow_,       // [in]  #ld
      src->xtype,  // [in]  type
      common_      //       CHOLMOD common object
  );
  cholmod_sdmult(
      schur_,  // [in]  multiplier
      false,   // [in]  no-transpose multiplier
      done,    // [in]  scalar of multiplier
      dzero,   // [in]  scalar of product
      src,     // [in]  multiplicand
      dst,     // [i/o] product
      common_  //       CHOLMOD common object
  );

  return dst;
}

////////////////////////////////////////////////////////////////////////////////
// The constructor of Schur2ConjFunc                                          //
//                                                                            //
// Parameters:                                                                //
// factor: the factor of the separator matrix                                 //
// schur2: the second term of the Schur complement                            //
// common: the CHOLMOD common object                                          //
////////////////////////////////////////////////////////////////////////////////
LorascSolver::LorascSeparator::Schur2ConjFunc::Schur2ConjFunc(
    cholmod_factor* factor, cholmod_sparse* schur2, cholmod_common* common ) {
  common_ = common;
  factor_ = factor;
  schur2_ = schur2;
  nrow_   = schur2->nrow;
}

////////////////////////////////////////////////////////////////////////////////
// Apply the Schur2 conjugate                                                 //
//                                                                            //
// Parameters:                                                                //
// src: the source matrix                                                     //
//                                                                            //
// Return Value:                                                              //
// the destination matrix, equal to                                           //
//   inv(L_ss) * schur2_ * inv(L_ss)' * src                                   //
////////////////////////////////////////////////////////////////////////////////
cholmod_dense* LorascSolver::LorascSeparator::Schur2ConjFunc::operator()(
    cholmod_dense* src ) {
  // tmp := inv(L_ss)' * src
  auto dst = cholmod_solve(
      CHOLMOD_Lt,  // solve L'x=b
      factor_,     // [in]  factor
      src,         // [in]  right-hand-side
      common_      //       CHOLMOD common object
  );
  auto tmp = cholmod_solve(
      CHOLMOD_Pt,  // solve x=P'b
      factor_,     // [in]  factor
      dst,         // [in]  right-hand-side
      common_      //       CHOLMOD common object
  );

  // dst := schur2_ * tmp
  cholmod_sdmult(
      schur2_,  // [in]  multiplier
      false,    // [in]  no-transpose multiplier
      done,     // [in]  scalar of multiplier
      dzero,    // [in]  scalar of product
      tmp,      // [in]  multiplicand
      dst,      // [i/o] product
      common_   //       CHOLMOD common object
  );
  cholmod_free_dense(&tmp, common_);

  // dst := inv(L_ss) * dst
  tmp = cholmod_solve(
      CHOLMOD_P,  // solve x=Pb
      factor_,    // [in]  factor
      dst,        // [in]  right-hand-side
      common_     //       CHOLMOD common object
  );
  cholmod_free_dense(&dst, common_);
  dst = cholmod_solve(
      CHOLMOD_L,  // solve Lx=b
      factor_,    // [in]  factor
      tmp,        // [in]  right-hand-side
      common_     //       CHOLMOD common object
  );
  cholmod_free_dense(&tmp, common_);

  return dst;
}

////////////////////////////////////////////////////////////////////////////////
// The constructor of SchurPrecFunc                                           //
//                                                                            //
// Parameters:                                                                //
// factor: the factor of the separator matrix                                 //
// eigvec: the eigenvectors of the Schur complement                           //
// eigval: the corrected eigenvalues of the Schur complement                  //
// rank:   the number of the eigenvalues                                      //
// common: the CHOLMOD common object                                          //
////////////////////////////////////////////////////////////////////////////////
LorascSolver::LorascSeparator::SchurPrecFunc::SchurPrecFunc(
    cholmod_factor* factor, cholmod_dense* eigvec, cholmod_sparse* eigval,
    size_t rank, cholmod_common* common ) {
  common_ = common;
  factor_ = factor;
  eigvec_ = eigvec;
  eigval_ = eigval;
  nrow_   = factor->n;
  rank_   = rank;
}

////////////////////////////////////////////////////////////////////////////////
// Apply the Schur preconditioner                                             //
//                                                                            //
// Parameters:                                                                //
// src: the source matrix                                                     //
//                                                                            //
// Return Value:                                                              //
// the destination matrix, equal to                                           //
//   inv(A_ss) * src + eigvec_ * eigval_ * eigvec_' * src                     //
////////////////////////////////////////////////////////////////////////////////
cholmod_dense* LorascSolver::LorascSeparator::SchurPrecFunc::operator()(
    cholmod_dense* src ) {
  const size_t nrhs = src->ncol;

  // dst := inv(A_ss) * src
  auto dst = cholmod_solve(
      CHOLMOD_A,  // [in]  solve Ax=b
      factor_,    // [in]  factor
      src,        // [in]  right-hand-side / solution
      common_     //       CHOLMOD common object
  );

  // Create temporaries
  auto tmp0 = cholmod_allocate_dense(
      rank_,       // [in]  #row
      nrhs,        // [in]  #col
      rank_,       // [in]  #ld
      src->xtype,  // [in]  the type of numerical values
      common_      //       CHOLMOD common object
  );
  auto tmp1 = cholmod_allocate_dense(
      rank_,       // [in]  #row
      nrhs,        // [in]  #col
      rank_,       // [in]  #ld
      src->xtype,  // [in]  the type of numerical values
      common_      //       CHOLMOD common object
  );

  // tmp0 := eigvec_' * src
  cblas_dgemm(
      CblasColMajor,            // [in]  column major
      CblasTrans,               // [in]  transpose multiplier
      CblasNoTrans,             // [in]  no-transpose multiplicand
      rank_,                    // [in]  #row: product
      nrhs,                     // [in]  #col: product
      nrow_,                    // [in]  #col: multiplier
      1.0,                      // [in]  scalar of multiplier
      reinterpret_cast
        <double*>(eigvec_->x),  // [in]  multiplier
      nrow_,                    // [in]  #ld: multiplier
      reinterpret_cast
        <double*>(src->x),      // [in]  multiplicand
      nrow_,                    // [in]  #ld: multiplicand
      0.0,                      // [in]  scalar of product
      reinterpret_cast
        <double*>(tmp0->x),     // [i/o] product
      rank_                     // [in]  #ld: product
  );

  // tmp1 := eigval_ * tmp0
  cholmod_sdmult(
      eigval_,  // [in]  multiplier
      false,    // [in]  no-transpose multiplier
      done,     // [in]  scalar of multiplier
      dzero,    // [in]  scalar of product
      tmp0,     // [in]  multiplicand
      tmp1,     // [i/o] product
      common_   //       CHOLMOD common object
  );

  // dst += eigvec_ * tmp0
  cblas_dgemm(
      CblasColMajor,            // [in]  column major
      CblasNoTrans,             // [in]  no-transpose multiplier
      CblasNoTrans,             // [in]  no-transpose multiplicand
      nrow_,                    // [in]  #row: product
      nrhs,                     // [in]  #col: product
      rank_,                    // [in]  #col: multiplier
      1.0,                      // [in]  scalar of multiplier
      reinterpret_cast
        <double*>(eigvec_->x),  // [in]  multiplier
      nrow_,                    // [in]  #ld: multiplier
      reinterpret_cast
        <double*>(tmp1->x),     // [in]  multiplicand
      rank_,                    // [in]  #ld: multiplicand
      1.0,                      // [in]  scalar of product
      reinterpret_cast
        <double*>(dst->x),      // [i/o] product
      nrow_                     // [in]  #ld of product
  );

  // Delete temporaries
  cholmod_free_dense(&tmp0, common_);
  cholmod_free_dense(&tmp1, common_);

  return dst;
}

}
