////////////////////////////////////////////////////////////////////////////////
// Robust Algebraic Schur Complement Preconditioners                          //
//     Based on Low Rank Corrections                                          //
//                                                                            //
// lorasc_domain.cpp                                                          //
// The struct LorascDomain                                                    //
//                                                                            //
// Author: emfo<emfomy@gmail.com>                                             //
////////////////////////////////////////////////////////////////////////////////

#include "lorasc_domain.hpp"

////////////////////////////////////////////////////////////////////////////////
// The namespace lorasc                                                       //
////////////////////////////////////////////////////////////////////////////////
namespace lorasc {

////////////////////////////////////////////////////////////////////////////////
// The manual constructor of LorascDomain                                     //
//                                                                            //
// Parameters:                                                                //
// common: the CHOLMOD common object                                          //
////////////////////////////////////////////////////////////////////////////////
void LorascSolver::LorascDomain::Construct( cholmod_common* common ) {
  common_ = common;
  matrix_ = nullptr;
  factor_ = nullptr;
  border_ = nullptr;
  perm_ = nullptr;
}

////////////////////////////////////////////////////////////////////////////////
// The manual destructor of LorascDomain                                      //
////////////////////////////////////////////////////////////////////////////////
void LorascSolver::LorascDomain::Destruct() {
  cholmod_free_sparse(&matrix_, common_);
  cholmod_free_factor(&factor_, common_);
  cholmod_free_sparse(&border_, common_);
  delete perm_;
}

////////////////////////////////////////////////////////////////////////////////
// Bisect the interior matrix                                                 //
//                                                                            //
// Input Parameters:                                                          //
// inc:    the increacment                                                    //
// perm_s: the permutation of separator entries                               //
// size_s: the number of separator entries                                    //
//                                                                            //
// Output Parameters:                                                         //
// perm_s: the updated permutation of separator entries                       //
// size_s: the updated number of separator entries                            //
////////////////////////////////////////////////////////////////////////////////
void LorascSolver::LorascDomain::Bisect( int inc,
                                         int* perm_s,
                                         size_t& size_s ) {
  int* part   = new int[this->nrow_];
  int* index0 = new int[this->nrow_];
  int* index1 = new int[this->nrow_];
  int* index2 = new int[this->nrow_];
  size_t size0, size1, size2;

  for ( inc/=2; inc >= 1; inc/=2 ) {
    auto that = this+inc;
    auto matrix_temp = this->matrix_;
    auto perm_temp = this->perm_;
    this->perm_ = new int[this->nrow_];
    that->perm_ = new int[this->nrow_];

    // Make matrix symmetric
    auto upper_temp = cholmod_copy(
        matrix_temp,  // [in]  source
        1,            // [in]  upper symmetric storage
        1,            // [in]  copy numerical values
        common_       //       CHOLMOD common object
    );

    // Bisection
    cholmod_bisect(
        upper_temp,  // [in]  matrix
        nullptr,     // [in]  full column subset
        NULL,        // [in]  size: column subset
        true,        // [in]  compress graph first
        part,        // [out] partition
        common_      //       CHOLMOD common object
    );
    cholmod_free_sparse(&upper_temp, common_);

    // Find indices
    size0 = 0, size1 = 0, size2 = 0;
    for ( int j = 0; j < this->nrow_; ++j ) {
      switch ( part[j] ) {
       case 0: // left domain
        index0[size0] = j;
        this->perm_[size0] = perm_temp[j];
        ++size0;
        break;
       case 1: // right domain
        index1[size1] = j;
        that->perm_[size1] = perm_temp[j];
        ++size1;
        break;
       case 2: // separator
        index2[size2++] = j;
        perm_s[size_s++] = perm_temp[j];
        break;
      }
    }

    // Copy matrices
    this->matrix_ = cholmod_submatrix(
        matrix_temp,  // [in]  source
        index0,       // [in]  row subset
        size0,        // [in]  size: row subset
        index0,       // [in]  column subset
        size0,        // [in]  size: column subset
        true,         // [in]  copy numerical values
        false,        // [in]  not sort destination columns
        common_       //       CHOLMOD common object
    );
    that->matrix_ = cholmod_submatrix(
        matrix_temp,  // [in]  source
        index1,       // [in]  row subset
        size1,        // [in]  size: row subset
        index1,       // [in]  column subset
        size1,        // [in]  size: column subset
        true,         // [in]  copy numerical values
        false,        // [in]  not sort destination columns
        common_       //       CHOLMOD common object
    );
    this->nrow_ = size0;
    that->nrow_ = size1;

    // Delete temporaries
    cholmod_free_sparse(&matrix_temp, common_);
    delete perm_temp;

    // Bisect right domain
    if ( inc > 1 ) {
      that->Bisect(
          inc,     // [in]  the increacment
          perm_s,  // [i/o] the permutation of separator entries
          size_s   // [i/o] the number of separator entries
      );
    }
  }

  delete[] part;
}

}
