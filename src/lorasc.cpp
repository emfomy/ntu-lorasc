////////////////////////////////////////////////////////////////////////////////
// Robust Algebraic Schur Complement Preconditioners                          //
//     Based on Low Rank Corrections                                          //
//                                                                            //
// lorasc.cpp                                                                 //
// The header file of the class Lorasc                                        //
//                                                                            //
// Author: emfo<emfomy@gmail.com>                                             //
////////////////////////////////////////////////////////////////////////////////

#include "lorasc.hpp"
#include "lorasc_solver.hpp"

////////////////////////////////////////////////////////////////////////////////
// The namespace lorasc                                                       //
////////////////////////////////////////////////////////////////////////////////
namespace lorasc {

////////////////////////////////////////////////////////////////////////////////
// The constructor of Lorasc                                                  //
//                                                                            //
// Parameters:                                                                //
// num_level: the number of dissection levels                                 //
// common: the CHOLMOD common object                                          //
////////////////////////////////////////////////////////////////////////////////
Lorasc::Lorasc( const size_t num_level, cholmod_common* common ) {
  solver_ = new LorascSolver( num_level, common );
}

////////////////////////////////////////////////////////////////////////////////
// The destructor of Lorasc                                                   //
////////////////////////////////////////////////////////////////////////////////
Lorasc::~Lorasc() {
  delete solver_;
}

////////////////////////////////////////////////////////////////////////////////
// Read a matrix                                                              //
//                                                                            //
// Parameters:                                                                //
// matrix: a sparse symmetric positive definite matrix                        //
////////////////////////////////////////////////////////////////////////////////
void Lorasc::Read( cholmod_sparse* matrix ) {
  solver_->Read(matrix);
}

////////////////////////////////////////////////////////////////////////////////
// Solve linear system                                                        //
//                                                                            //
// Input Parameters:                                                          //
// vector: the right-hand-side vectors                                        //
//                                                                            //
// Output Parameters:                                                         //
// vector: replaced by the solution vectors                                   //
////////////////////////////////////////////////////////////////////////////////
void Lorasc::Solve( cholmod_dense* vector ) {
  solver_->Solve(vector);
}

}
