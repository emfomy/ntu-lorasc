////////////////////////////////////////////////////////////////////////////////
// Robust Algebraic Schur Complement Preconditioners                          //
//     Based on Low Rank Corrections                                          //
//                                                                            //
// lorasc_auxiliary.hpp                                                       //
// The header file of the auxiliary routines                                  //
//                                                                            //
// Author: emfo<emfomy@gmail.com>                                             //
////////////////////////////////////////////////////////////////////////////////

#ifndef LORASC_LORASC_AUXILIARY_HPP_

#define LORASC_LORASC_AUXILIARY_HPP_

#include "cholmod.h"
#include "lorasc_const.hpp"

////////////////////////////////////////////////////////////////////////////////
// The namespace lorasc                                                       //
////////////////////////////////////////////////////////////////////////////////
namespace lorasc {

////////////////////////////////////////////////////////////////////////////////
// The class of a square multiplier function handle for dense matrices        //
////////////////////////////////////////////////////////////////////////////////
class LorascDenseFunc {
 protected:
  // the number of rows of the output matrix
  size_t nrow_;

 public:
  virtual cholmod_dense* operator()( cholmod_dense* src ) = 0;
  inline size_t nrow() { return nrow_; };
};

////////////////////////////////////////////////////////////////////////////////
// Approximate low rank eigenvalue decomposition                              //
// Use a randomized algorithm for low rank eigenvalue decomposition           //
//   approximation of a symmetric positive definite matrix                    //
////////////////////////////////////////////////////////////////////////////////
void LorascEigen( LorascDenseFunc& func,
                  const size_t rank,
                  cholmod_dense*& eigvec,
                  cholmod_sparse*& eigval,
                  cholmod_common* common );

////////////////////////////////////////////////////////////////////////////////
// The preconditioned conjugate gradient method                               //
////////////////////////////////////////////////////////////////////////////////
void LorascPCG( LorascDenseFunc& func,
                LorascDenseFunc& prec,
                cholmod_dense*& rhs,
                cholmod_dense*& sol,
                cholmod_common* common );

}

#endif
