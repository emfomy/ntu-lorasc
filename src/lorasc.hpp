////////////////////////////////////////////////////////////////////////////////
// Robust Algebraic Schur Complement Preconditioners                          //
//     Based on Low Rank Corrections                                          //
//                                                                            //
// lorasc.hpp                                                                 //
// The main header file                                                       //
//                                                                            //
// Author: emfo<emfomy@gmail.com>                                             //
////////////////////////////////////////////////////////////////////////////////

#ifndef LORASC_LORASC_HPP_

#define LORASC_LORASC_HPP_

#include "cholmod.h"

////////////////////////////////////////////////////////////////////////////////
// The namespace lorasc                                                       //
////////////////////////////////////////////////////////////////////////////////
namespace lorasc {

////////////////////////////////////////////////////////////////////////////////
// The main class of LORASC                                                   //
////////////////////////////////////////////////////////////////////////////////
class Lorasc {
 private:
  // The LORASC solver
  class LorascSolver* solver_;

 public:
  ////////////////////////////////////////////////////////////////////////////
  // The constructor of Lorasc                                              //
  //                                                                        //
  // Parameters:                                                            //
  // num_level: the number of dissection levels                             //
  // common: the CHOLMOD common object                                      //
  ////////////////////////////////////////////////////////////////////////////
  Lorasc( const size_t num_level, cholmod_common* common );

  ////////////////////////////////////////////////////////////////////////////
  // The destructor of Lorasc                                               //
  ////////////////////////////////////////////////////////////////////////////
  ~Lorasc();

  ////////////////////////////////////////////////////////////////////////////
  // Read a matrix                                                          //
  //                                                                        //
  // Parameters:                                                            //
  // matrix: the matrix, sparse symmetric positive definite                 //
  ////////////////////////////////////////////////////////////////////////////
  void Read( cholmod_sparse* matrix );

  ////////////////////////////////////////////////////////////////////////////
  // Solve linear system                                                    //
  //                                                                        //
  // Input Parameters:                                                      //
  // vector: the right-hand-side vectors                                    //
  //                                                                        //
  // Output Parameters:                                                     //
  // vector: replaced by the solution vectors                               //
  ////////////////////////////////////////////////////////////////////////////
  void Solve( cholmod_dense* vector );
};

}

#endif
