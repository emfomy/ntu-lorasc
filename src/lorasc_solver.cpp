////////////////////////////////////////////////////////////////////////////////
// Robust Algebraic Schur Complement Preconditioners                          //
//     Based on Low Rank Corrections                                          //
//                                                                            //
// lorasc_solver.cpp                                                          //
// The class LorascSolver                                                     //
//                                                                            //
// Author: emfo<emfomy@gmail.com>                                             //
////////////////////////////////////////////////////////////////////////////////

#include "lorasc_solver.hpp"
#include <numeric>
#include "mkl.h"
#include "lorasc_domain.hpp"
#include "lorasc_separator.hpp"

////////////////////////////////////////////////////////////////////////////////
// The namespace lorasc                                                       //
////////////////////////////////////////////////////////////////////////////////
namespace lorasc {

////////////////////////////////////////////////////////////////////////////////
// The constructor of LorascSolver                                            //
//                                                                            //
// Parameters:                                                                //
// num_level: the number of dissection levels                                 //
// common: the CHOLMOD common object                                          //
////////////////////////////////////////////////////////////////////////////////
LorascSolver::LorascSolver( const size_t num_level, cholmod_common* common ) {
  // Start CHOLMOD
  common_ = common;
  common_->final_ll = true;

  // Initizlise constants
  num_level_  = num_level;
  num_domain_ = 1 << num_level;

  // Construct domains
  domain_ = new LorascDomain[num_domain_]();
  separator_ = new LorascSeparator();
  for ( int i = 0; i < num_domain_; ++i ) {
    domain_[i].Construct(common_);
  }
  separator_->Construct(common_);
}

////////////////////////////////////////////////////////////////////////////////
// The destructor of LorascSolver                                             //
////////////////////////////////////////////////////////////////////////////////
LorascSolver::~LorascSolver() {
  for ( int i = 0; i < num_domain_; ++i ) {
    domain_[i].Destruct();
  }
  separator_->Destruct();
  delete[] domain_;
  delete separator_;
}

////////////////////////////////////////////////////////////////////////////////
// Read a matrix                                                              //
//                                                                            //
// Parameters:                                                                //
// matrix: a sparse symmetric positive definite matrix                        //
////////////////////////////////////////////////////////////////////////////////
void LorascSolver::Read( cholmod_sparse* matrix ) {
  nrow_ = matrix->nrow;

  ////////////////////////////////////////////////////////////////////////////
  // Nested dissect the matrix                                              //
  ////////////////////////////////////////////////////////////////////////////
  int *perm_s = new int[nrow_];
  size_t size_s = 0;

  // Change source matrix to full storage
  auto matrix_full = cholmod_copy(
      matrix,  // [in]  source
      0,       // [in]  full storage
      1,       // [in]  copy numerical values
      common_  //       CHOLMOD common object
  );

  // Put matrix into domain 0
  domain_[0].matrix_ = cholmod_copy_sparse(
      matrix_full,  // [in]  source
      common_       //       CHOLMOD common object
  );
  domain_[0].nrow_ = nrow_;
  domain_[0].perm_ = new int[nrow_];
  std::iota(
      domain_[0].perm_,        // first position
      domain_[0].perm_+nrow_,  // last position
      0                        // start value
  );

  // Bisect domains
  domain_[0].Bisect(
      num_domain_,  // [in]  the increacment
      perm_s,       // [i/o] the permutation of separator entries
      size_s        // [i/o] the number of separator entries
  );

  // Change interior matrices storage
  for ( int i = 0; i < num_domain_; ++i ) {
    auto matrix_temp = domain_[i].matrix_;
    domain_[i].matrix_ = cholmod_copy(
        matrix_temp,  // [in]  source
        1,            // [in]  upper symmetric storage
        1,            // [in]  copy numerical values
        common_       //       CHOLMOD common object
    );
    cholmod_free_sparse(&matrix_temp, common_);
  }

  // Put border matrices into domains and change storage
  for ( int i = 0; i < num_domain_; ++i ) {
    domain_[i].border_ = cholmod_submatrix(
        matrix_full,       // [in]  source
        domain_[i].perm_,  // [in]  row subset
        domain_[i].nrow_,  // [in]  size: row subset
        perm_s,            // [in]  size: row subset
        size_s,            // [in]  size: column subset
        true,              // [in]  copy numerical values
        false,             // [in]  not sort destination columns
        common_            //       CHOLMOD common object
    );
  }

  // Put separator matrix into separator and change storage
  auto matrix_temp = cholmod_submatrix(
      matrix_full,  // [in]  source
      perm_s,       // [in]  row subset
      size_s,       // [in]  size: row subset
      perm_s,       // [in]  size: row subset
      size_s,       // [in]  size: column subset
      true,         // [in]  copy numerical values
      false,        // [in]  not sort destination columns
      common_       //       CHOLMOD common object
  );
  separator_->matrix_ = cholmod_copy(
      matrix_temp,  // [in]  source
      1,            // [in]  upper symmetric storage
      1,            // [in]  copy numerical values
      common_       //       CHOLMOD common object
  );
  separator_->nrow_ = size_s;
  separator_->perm_ = perm_s;
  cholmod_free_sparse(&matrix_temp, common_);

  // Delete matrix
  cholmod_free_sparse(&matrix_full, common_);

  ////////////////////////////////////////////////////////////////////////////
  // Compute the Cholesky decomposition of matrices                         //
  ////////////////////////////////////////////////////////////////////////////

  // Compute Cholesky decomposition for interior matrices
  for ( int i = 0; i < num_domain_; ++i ) {
    domain_[i].factor_ = cholmod_analyze(
        domain_[i].matrix_,  // [in]  matrix
        common_              //       CHOLMOD common object
    );
    cholmod_factorize(
        domain_[i].matrix_,  // [in]  matrix
        domain_[i].factor_,  // [out] factor
        common_              //       CHOLMOD common object
    );
  }

  // Compute Cholesky decomposition for separator matrix
  separator_->factor_ = cholmod_analyze(
      separator_->matrix_,  // [in]  matrix
      common_               //       CHOLMOD common object
  );
  cholmod_factorize(
      separator_->matrix_,  // [in]  matrix
      separator_->factor_,  // [out] factor
      common_               //       CHOLMOD common object
  );

  ////////////////////////////////////////////////////////////////////////////
  // Compute the Schur complement                                           //
  //                                                                        //
  // Note:                                                                  //
  // schur2_ := sum( A_si * inv(A_ii) * A_is )                              //
  // schur_ := A_ss - schur2_                                               //
  ////////////////////////////////////////////////////////////////////////////

  // Initialize schur2
  auto schur2_temp = cholmod_spzeros(
      separator_->nrow_,  // [in]  #row
      separator_->nrow_,  // [in]  #col
      0,                  // [in]  #non-zero
      CHOLMOD_REAL,       // [in]  real (double) type
      common_             //       CHOLMOD common object
  );
  separator_->schur2_ = cholmod_copy(
      schur2_temp,  // [in]  source
      1,            // [in]  upper symmetric storage
      1,            // [in]  copy numerical values
      common_       //       CHOLMOD common object
  );
  cholmod_free_sparse(&schur2_temp, common_);

  // Compute the second term of the Schur complement
  for ( int i = 0; i < num_domain_; ++i ) {
    // schur2_ += A_si * inv(A_ii) * A_is
    auto matrix_temp0 = cholmod_spsolve(
        CHOLMOD_P,           // [in]  solve x=Pb
        domain_[i].factor_,  // [in]  factor
        domain_[i].border_,  // [in]  right-hand-side
        common_              //       CHOLMOD common object
    );
    auto matrix_temp1 = cholmod_spsolve(
        CHOLMOD_L,           // [in]  solve Lx=b
        domain_[i].factor_,  // [in]  factor
        matrix_temp0,        // [in]  right-hand-side
        common_              //       CHOLMOD common object
    );
    auto matrix_temp2 = cholmod_transpose(
        matrix_temp1,  // [in]  source
        1,             // [in]  transpose
        common_        //       CHOLMOD common object
    );
    auto matrix_temp3 = cholmod_aat(
        matrix_temp2,  // [in]  multiplcand
        nullptr,       // [in]  full column subset
        NULL,          // [in]  size: column subset
        1,             // [in]  compute numerical values
        common_        //       CHOLMOD common object
    );
    auto matrix_temp4 = cholmod_copy(
        matrix_temp3,  // [in]  source
        1,             // [in]  upper symmetric storage
        1,             // [in]  copy numerical values
        common_        //       CHOLMOD common object
    );
    schur2_temp = separator_->schur2_;
    separator_->schur2_ = cholmod_add(
        schur2_temp,   // [in]  augend
        matrix_temp4,  // [in]  addend
        done,          // [in]  scalar of augend
        done,          // [in]  scalar of addend
        true,          // [in]  compute numerical values
        false,         // [in]  not sort sum columns
        common_        //       CHOLMOD common object
    );

    // Delete temporaries
    cholmod_free_sparse(&matrix_temp0, common_);
    cholmod_free_sparse(&matrix_temp1, common_);
    cholmod_free_sparse(&matrix_temp2, common_);
    cholmod_free_sparse(&matrix_temp3, common_);
    cholmod_free_sparse(&matrix_temp4, common_);
    cholmod_free_sparse(&schur2_temp, common_);
  }

  // Create the function handle of the Schur complement
  separator_->schur_func_ =
      new LorascSolver::LorascSeparator::SchurFunc(
        separator_->matrix_,  // [in]  the separator matrix
        separator_->schur2_,  // [in]  the second term of the Schur complement
        common_               //       CHOLMOD common object
  );

  ////////////////////////////////////////////////////////////////////////////
  // Compute the preconditioner of the Schur complement                     //
  ////////////////////////////////////////////////////////////////////////////

  // Create the function handle of the Schur2 conjugate
  separator_->schur2_conj_func_ =
      new LorascSolver::LorascSeparator::Schur2ConjFunc(
        separator_->factor_,  // [in]  the factor of the separator matrix
        separator_->schur2_,  // [in]  the second term of the Schur complement
        common_               //       CHOLMOD common object
  );

  // Approximate low rank generalized eigenvalue decomposition
  size_t rank = num_domain_;
  LorascEigen(
      *separator_->schur2_conj_func_,  // [in]  function handle
      rank,                            // [in]  approximate rank
      separator_->eigvec_,             // [out] approximate eigenvectors
      separator_->eigval_,             // [out] approximate eigenvalues
      common_                          //       CHOLMOD common object
  );

  // Compute the eigenvectors
  // eigvec_ := normalize(inv(L_ss)' * eigvec_)
  auto eig_temp = cholmod_solve(
      CHOLMOD_Lt,           // [in]  solve L'x=b
      separator_->factor_,  // [in]  factor
      separator_->eigvec_,  // [in]  right-hand-side
      common_               //       CHOLMOD common object
  );
  cholmod_free_dense(&separator_->eigvec_, common_);
  separator_->eigvec_ = cholmod_solve(
      CHOLMOD_Pt,           // [in]  solve x=P'b
      separator_->factor_,  // [in]  factor
      eig_temp,             // [in]  right-hand-side
      common_               //       CHOLMOD common object
  );
  cholmod_free_dense(&eig_temp, common_);
  for ( int j = 0; j < rank; ++j ) {
    auto dtemp = cblas_dnrm2(
        separator_->nrow_,                   // [in]  #entry
        reinterpret_cast
          <double*>(separator_->eigvec_->x)
          + j*separator_->nrow_,             // [in]  array
        1                                    // [in]  #inc: array
    );
    cblas_dscal(
        separator_->nrow_,                   // [in]  #entry
        1/dtemp,                             // [in]  scalar
        reinterpret_cast
          <double*>(separator_->eigvec_->x)
          + j*separator_->nrow_,             // [i/o] array
        1                                    // [in]  #inc: array
    );
  }

  // Correct the eigenvalues
  // eigval_ := max(1-eigval_)/(1-eigval_) - 1
  auto itemp = cblas_idamin(
      rank,                                 // [in]  #entry
      reinterpret_cast
        <double*>(separator_->eigval_->x),  // [i/o] array
      1                                     // [in]  #inc: array
  );
  auto dtemp = reinterpret_cast<double*>(separator_->eigval_->x)[itemp];
  vdLinearFrac(
      rank,                                 // [in]  #entry
      reinterpret_cast
        <double*>(separator_->eigval_->x),  // [in]  dividend
      reinterpret_cast
        <double*>(separator_->eigval_->x),  // [in]  divisor
      1.0,                                  // [in]  scalar of dividend
      -dtemp,                               // [in]  shift of dividend
      -1.0,                                 // [in]  scalar of divisor
      1.0,                                  // [in]  shift of divisor
      reinterpret_cast
        <double*>(separator_->eigval_->x)   // [out] quotient
  );

  // Create the function handle of the Schur preconditioner
  separator_->schur_prec_func_ =
      new LorascSolver::LorascSeparator::SchurPrecFunc(
        separator_->factor_,  // [in]  factor of separator matrix
        separator_->eigvec_,  // [in]  eigenvectors
        separator_->eigval_,  // [in]  corrected eigenvalues
        rank,                 // [in]  number of eigenvalues
        common_               //       CHOLMOD common object
  );
}

////////////////////////////////////////////////////////////////////////////////
// Solve linear system                                                        //
//                                                                            //
// Input Parameters:                                                          //
// vector: the right-hand-side vectors                                        //
//                                                                            //
// Output Parameters:                                                         //
// vector: replaced by the solution vectors                                   //
////////////////////////////////////////////////////////////////////////////////
void LorascSolver::Solve( cholmod_dense* vector ) {

  ////////////////////////////////////////////////////////////////////////////
  // Put right-hand-side vectors into domains                               //
  ////////////////////////////////////////////////////////////////////////////
  for ( int i = 0; i < num_domain_; ++i ) {
    domain_[i].vec_b_ = cholmod_allocate_dense(
        domain_[i].nrow_,  // [in]  #row
        vector->ncol,      // [in]  #col
        domain_[i].nrow_,  // [in]  #ld
        CHOLMOD_REAL,      // [in]  real (double) type
        common_            //       CHOLMOD common object
    );
    for ( int j = 0; j < domain_[i].nrow_; ++j ) {
      cblas_dcopy(
          vector->ncol,                      // [in]  #entry
          reinterpret_cast
            <double*>(vector->x)
            + domain_[i].perm_[j],           // [in]  #source
          vector->nrow,                      // [in]  #inc: source
          reinterpret_cast
            <double*>(domain_[i].vec_b_->x)
            + j,                             // [out] #destination
          domain_[i].nrow_                   // [in]  #inc: destination
      );
    }
  }
  separator_->vec_b_ = cholmod_allocate_dense(
      separator_->nrow_,  // [in]  #row
      vector->ncol,       // [in]  #col
      separator_->nrow_,  // [in]  #ld
      CHOLMOD_REAL,       // [in]  real (double) type
      common_             //       CHOLMOD common object
  );
  for ( int j = 0; j < separator_->nrow_; ++j ) {
    cblas_dcopy(
        vector->ncol,                       // [in]  #entry
        reinterpret_cast
          <double*>(vector->x)
          + separator_->perm_[j],           // [in]  #source
        vector->nrow,                       // [in]  #inc: source
        reinterpret_cast
          <double*>(separator_->vec_b_->x)
          + j,                              // [out] #destination
        separator_->nrow_                   // [in]  #inc: destination
    );
  }

  ////////////////////////////////////////////////////////////////////////////
  // Apply lower LORASC preconditionor                                      //
  ////////////////////////////////////////////////////////////////////////////
  // z_s := b_s
  separator_->vec_z_ = separator_->vec_b_;
  for ( int i = 0; i < num_domain_; ++i ) {
    // y_i := inv(A_ii) * b_i
    domain_[i].vec_y_ = cholmod_solve(
        CHOLMOD_A,           // [in]  solve Ax=b
        domain_[i].factor_,  // [in]  factor
        domain_[i].vec_b_,   // [in]  right-hand-side
        common_              //       CHOLMOD common object
    );

    // z_s -= A_si * y_i;
    cholmod_sdmult(
        domain_[i].border_,  // [in]  multiplier
        true,                // [in]  transpose multiplier
        dnone,               // [in]  scalar of multiplier
        done,                // [in]  scalar of product
        domain_[i].vec_y_,   // [in]  multiplicand
        separator_->vec_z_,  // [i/o] product
        common_              //       CHOLMOD common object
    );
  }
  // y_s := inv(schur_prec_) * b_s
  separator_->vec_y_ = (*separator_->schur_prec_func_)(separator_->vec_z_);

  ////////////////////////////////////////////////////////////////////////////
  // Apply upper LORASC preconditionor                                      //
  ////////////////////////////////////////////////////////////////////////////
  for ( int i = 0; i < num_domain_; ++i ) {
    // tmp := A_is * y_s
    auto tmp = domain_[i].vec_b_;
    cholmod_sdmult(
        domain_[i].border_,  // [in]  multiplier
        false,               // [in]  no-transpose multiplier
        done,                // [in]  scalar of multiplier
        dzero,               // [in]  scalar of product
        separator_->vec_y_,  // [in]  multiplicand
        tmp,                 // [i/o] product
        common_              //       CHOLMOD common object
    );

    // c_i := inv(A_ii) * tmp
    domain_[i].vec_c_ = cholmod_solve(
        CHOLMOD_A,           // [in]  solve Ax=b
        domain_[i].factor_,  // [in]  factor
        tmp,                 // [in]  right-hand-side
        common_              //       CHOLMOD common object
    );

    // d_i := y_i - c_i
    domain_[i].vec_d_ = domain_[i].vec_y_;
    vdSub(
        domain_[i].vec_d_->nzmax,           // [in]  #entry
        reinterpret_cast
          <double*>(domain_[i].vec_y_->x),  // [in]  minuend
        reinterpret_cast
          <double*>(domain_[i].vec_c_->x),  // [in]  subtrahend
        reinterpret_cast
          <double*>(domain_[i].vec_d_->x)   // [out] difference
    );
  }

  ////////////////////////////////////////////////////////////////////////////
  // Solve linear system                                                    //
  ////////////////////////////////////////////////////////////////////////////

  // x_s := inv(schur) * z_s, using inv(schur_prec) as preconditioner
  LorascPCG(
      *separator_->schur_func_,       // [in]  matrix function handle
      *separator_->schur_prec_func_,  // [in]  inverse preconditioner
      separator_->vec_z_,             // [in]  right-hand-side vectors
      separator_->vec_x_,             // [out] solution vectors
      common_                         //       CHOLMOD common object
  );

  // y_s -= x_s
  vdSub(
      separator_->vec_y_->nzmax,           // [in]  #entry
      reinterpret_cast
        <double*>(separator_->vec_y_->x),  // [in]  minuend
      reinterpret_cast
        <double*>(separator_->vec_x_->x),  // [in]  subtrahend
      reinterpret_cast
        <double*>(separator_->vec_y_->x)   // [out] difference
  );

  for ( int i = 0; i < num_domain_; ++i ) {
    // tmp = A_is * y_s
    auto tmp = domain_[i].vec_b_;
    cholmod_sdmult(
        domain_[i].border_,  // [in]  multiplier
        false,               // [in]  no-transpose multiplier
        done,                // [in]  scalar of multiplier
        dzero,               // [in]  scalar of product
        separator_->vec_y_,  // [in]  multiplicand
        tmp,                 // [i/o] product
        common_              //       CHOLMOD common object
    );

    // c_i := inv(A_ii) * tmp
    domain_[i].vec_c_ = cholmod_solve(
        CHOLMOD_A,           // [in]  solve Ax=b
        domain_[i].factor_,  // [in]  factor
        tmp,                 // [in]  right-hand-side
        common_              //       CHOLMOD common object
    );

    // x_i := d_i + c_i
    domain_[i].vec_x_ = domain_[i].vec_d_;
    vdAdd(
        domain_[i].vec_x_->nzmax,             // [in]  #entry
        reinterpret_cast
            <double*>(domain_[i].vec_d_->x),  // [in]  summand
        reinterpret_cast
            <double*>(domain_[i].vec_c_->x),  // [in]  summand
        reinterpret_cast
            <double*>(domain_[i].vec_x_->x)   // [out] sum
    );
  }

  ////////////////////////////////////////////////////////////////////////////
  // Extract vectors from domains                                           //
  ////////////////////////////////////////////////////////////////////////////
  for ( int i = 0; i < num_domain_; ++i ) {
    for ( int j = 0; j < domain_[i].nrow_; ++j ) {
      cblas_dcopy(
          vector->ncol,                      // [in]  #entry
          reinterpret_cast
            <double*>(domain_[i].vec_x_->x)
            + j,                             // [in]  #source
          domain_[i].nrow_,                  // [in]  #inc: source
          reinterpret_cast
            <double*>(vector->x)
            + domain_[i].perm_[j],           // [out] #destination
          vector->nrow                       // [in]  #inc: destination
      );
    }
  }
  for ( int j = 0; j < separator_->nrow_; ++j ) {
    cblas_dcopy(
        vector->ncol,                       // [in]  #entry
        reinterpret_cast
          <double*>(separator_->vec_x_->x)
          + j,                              // [in]  #source
        separator_->nrow_,                  // [in]  #inc: source
        reinterpret_cast
          <double*>(vector->x)
          + separator_->perm_[j],           // [out] #destination
        vector->nrow                        // [in]  #inc: destination
    );
  }

  ////////////////////////////////////////////////////////////////////////////
  // Delete matrices                                                        //
  ////////////////////////////////////////////////////////////////////////////
  for ( int i = 0; i < num_domain_; ++i ) {
    cholmod_free_dense(&domain_[i].vec_b_, common_);
    domain_[i].vec_y_ = nullptr;
    cholmod_free_dense(&domain_[i].vec_c_, common_);
    domain_[i].vec_d_ = nullptr;
    cholmod_free_dense(&domain_[i].vec_x_, common_);
  }
  cholmod_free_dense(&separator_->vec_b_, common_);
  separator_->vec_z_ = nullptr;
  cholmod_free_dense(&separator_->vec_y_, common_);
  cholmod_free_dense(&separator_->vec_x_, common_);
}

}
