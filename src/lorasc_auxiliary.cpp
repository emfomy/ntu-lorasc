////////////////////////////////////////////////////////////////////////////////
// Robust Algebraic Schur Complement Preconditioners                          //
//     Based on Low Rank Corrections                                          //
//                                                                            //
// lorasc_auxiliary.cpp                                                       //
// The auxiliary routines                                                     //
//                                                                            //
// Author: emfo<emfomy@gmail.com>                                             //
////////////////////////////////////////////////////////////////////////////////

#include "lorasc_auxiliary.hpp"
#include <algorithm>
#include <iostream>
#include "mkl.h"
#include "mkl_lapacke.h"

////////////////////////////////////////////////////////////////////////////////
// The namespace lorasc                                                       //
////////////////////////////////////////////////////////////////////////////////
namespace lorasc {

// the maximum iteration number
const int kMaxIter = 64;

////////////////////////////////////////////////////////////////////////////////
// Approximate low rank eigenvalue decomposition                              //
// Use a randomized algorithm for low rank eigenvalue decomposition           //
//   approximation of a symmetric positive definite matrix                    //
//                                                                            //
// Input Parameters:                                                          //
// func: the function handle                                                  //
// rank: the approximate rank of the matrix                                   //
//                                                                            //
// Output Parameters:                                                         //
// eigvec: the approximate eigenvectors                                       //
// eigval: the approximate eigenvalues                                        //
//                                                                            //
// Other Parameters:                                                          //
// common: the CHOLMOD common object                                          //
//                                                                            //
// Algorithm:                                                                 //
// 1. Generate mat as (nrow by rank) normal random matrix                     //
// 2. Update mat := func(mat)                                                 //
// 3. Orthonormalize mat                                                      //
// 4. Repeat 2-3 for some iterations                                          //
// 5. Update mat := func(mat)                                                 //
// 6. Compute the SVD-decomposition of mat,                                   //
//      eigvec := left singular vectors, eigval := singular values            //
//                                                                            //
// Reference:                                                                 //
// Witten, R., & Candès, E. (2014). Randomized Algorithms for Low-Rank Matrix //
//   Factorizations: Sharp Performance Bounds. Algorithmica, 72(1).           //
//   http://doi.org/10.1007/s00453-014-9891-7                                 //
////////////////////////////////////////////////////////////////////////////////
void LorascEigen( LorascDenseFunc& func,
                  const size_t rank,
                  cholmod_dense*& eigvec,
                  cholmod_sparse*& eigval,
                  cholmod_common* common ) {
  const size_t nrow = func.nrow();

  // Allocate return values
  eigvec = cholmod_allocate_dense(
      nrow,          // [in]  #row
      rank,          // [in]  #col
      nrow,          // [in]  #ld
      CHOLMOD_REAL,  // [in]  real (double) type
      common         //       CHOLMOD common object
  );
  eigval = cholmod_speye(
      rank,          // [in]  #row
      rank,          // [in]  #col
      CHOLMOD_REAL,  // [in]  real (double) type
      common         //       CHOLMOD common object
  );

  // Query recommended workspace size
  size_t lwork = 4*nrow;
  double query;

  LAPACKE_dgeqrf_work(
      LAPACK_COL_MAJOR,  // [in]  column major
      nrow,              // [in]  #row: matrix
      rank,              // [in]  #col: matrix
      nullptr,           //       (not using for query)
      nrow,              // [in]  #ld:  matrix
      nullptr,           //       (not using for query)
      &query,            // [out] recommand workspace size
      -1                 // [in]  query
  );
  lwork = std::max(lwork, static_cast<size_t>(query));

  LAPACKE_dorgbr_work(
      LAPACK_COL_MAJOR,  // [in]  column major
      'Q',               // [in]  left singular vectors
      nrow,              // [in]  #row: matrix
      rank,              // [in]  #col: to be computed
      rank,              // [in]  #col: matrix
      nullptr,           //       (not using for query)
      nrow,              // [in]  #ld:  matrix
      nullptr,           //       (not using for query)
      &query,            // [out] recommand workspace size
      -1                 // [in]  query
  );
  lwork = std::max(lwork, static_cast<size_t>(query));

  LAPACKE_dgebrd_work(
      LAPACK_COL_MAJOR,  // [in]  column major
      nrow,              // [in]  #row: matrix
      rank,              // [in]  #col: matrix
      nullptr,           //       (not using for query)
      nrow,              // [in]  #ld:  matrix
      nullptr,           //       (not using for query)
      nullptr,           //       (not using for query)
      nullptr,           //       (not using for query)
      nullptr,           //       (not using for query)
      &query,            // [out] recommand workspace size
      -1                 // [in]  query
  );
  lwork = std::max(lwork, static_cast<size_t>(query));

  // Create temporaries
  auto& mat = eigvec;
  auto vec_d = reinterpret_cast<double*>(eigval->x);
  auto vec_e = new double[rank-1];
  auto tau_q = new double[rank];
  auto tau_p = new double[rank];
  auto work = new double[lwork];

  // Sample a low rank mat matrix with
  //    independent mean-zero, unit-variance Gaussian entries
  LAPACKE_dlarnv(
      3,                   // [in]  normal (0, 1) distribution
      iseed,               // [in]  random seed
      nrow*rank,           // [in]  #entry
      reinterpret_cast
        <double*>(mat->x)  // [out] matrix
  );

  for ( int i = 1; i < kMaxIter; ++i ) {
    // mat := func(mat)
    auto temp = mat;
    mat = func(temp);
    cholmod_free_dense(&temp, common);

    // Orthonormalize matrix
    LAPACKE_dgeqrf_work(
        LAPACK_COL_MAJOR,     // [in]  column major
        nrow,                 // [in]  #row: matrix
        rank,                 // [in]  #col: matrix
        reinterpret_cast
          <double*>(mat->x),  // [i/o] matrix
        nrow,                 // [in]  #ld:  matrix
        tau_q,                // [out] details of orthonormal basis
        work,                 // [in]  workspace
        lwork                 // [in]  #entry: workspace
    );
    LAPACKE_dorgqr_work(
        LAPACK_COL_MAJOR,     // [in]  column major
        nrow,                 // [in]  #row: matrix
        rank,                 // [in]  #col: to be computed
        rank,                 // [in]  #col: matrix
        reinterpret_cast
          <double*>(mat->x),  // [i/o] matrix
        nrow,                 // [in]  #ld:  matrix
        tau_q,                // [in]  details of orthonormal basis
        work,                 // [in]  workspace
        lwork                 // [in]  #entry: workspace
    );
  }

  // mat := func(mat)
  auto temp = mat;
  mat = func(temp);
  cholmod_free_dense(&temp, common);

  // Find singular value decomposition mat
  LAPACKE_dgebrd_work(
      LAPACK_COL_MAJOR,     // [in]  column major
      nrow,                 // [in]  #row
      rank,                 // [in]  number of columns
      reinterpret_cast
        <double*>(mat->x),  // [i/o] matrix
      nrow,                 // [in]  leading dimension
      vec_d,                // [out] diagonals
      vec_e,                // [out] off-diagonals
      tau_q,                // [out] details of left singular vectors
      tau_p,                // [out] details of right singular vectors
      work,                 // [in]  workspace
      lwork                 // [in]  #entry: the workspace
  );
  LAPACKE_dorgbr_work(
      LAPACK_COL_MAJOR,     // [in]  column major
      'Q',                  // [in]  left singular vectors
      nrow,                 // [in]  #row: left singular vectors
      rank,                 // [in]  #col: to be computed
      rank,                 // [in]  #col: left singular vectors
      reinterpret_cast
        <double*>(mat->x),  // [i/o] left singular vectors
      nrow,                 // [in]  #ld: left singular vectors
      tau_q,                // [in]  details of left singular vectors
      work,                 // [in]  workspace
      lwork                 // [in]  #entry: workspace
  );
  LAPACKE_dbdsqr_work(
      LAPACK_COL_MAJOR,     // [in]  column major
      'U',                  // [in]  upper bidiagonal
      rank,                 // [in]  #: singular values
      0,                    //       (no right singular vectors)
      nrow,                 // [in]  #row: left singular vectors
      0,                    //       (no left-hand-side matrix)
      vec_d,                // [i/o] singular values
      vec_e,                // [in]  off-diagonals
      nullptr,              //       (no right singular vectors)
      1,                    //       (no right singular vectors)
      reinterpret_cast
        <double*>(mat->x),  // [i/o] left singular vectors
      nrow,                 // [in]  #ld: left singular vectors
      nullptr,              //       (no left-hand-side vectors)
      1,                    //       (no left-hand-side vectors)
      work                  // [in]  the workspace
  );

  // Delete temporaries
  delete[] vec_e;
  delete[] tau_q;
  delete[] tau_p;
  delete[] work;
}

////////////////////////////////////////////////////////////////////////////////
// The preconditioned conjugate gradient method                               //
//                                                                            //
// Input Parameters:                                                          //
// func: the matrix function handle (Afun)                                    //
// prec: the inverse preconditioner function handle (Mfun)                    //
// rhs:  the right-hand-side vectors (b)                                      //
//                                                                            //
// Output Parameters:                                                         //
// sol: the solution vectors (x)                                              //
//                                                                            //
// Other Parameters:                                                          //
// common: the CHOLMOD common object                                          //
//                                                                            //
// Algorithm:                                                                 //
// x := 0                                                                     //
// r := b - Afun(x)                                                           //
// REPEAT                                                                     //
//   z := Mfun(r)                                                             //
//   rho1 := r' * z                                                           //
//   IF first loop                                                            //
//     p := z                                                                 //
//   ELSE                                                                     //
//     p := z + (rho1/rho0) * p                                               //
//   END IF                                                                   //
//   q := Afun(p)                                                             //
//   alpha := rho1 / (p'*q)                                                   //
//   x += alpha * p                                                           //
//   r -= alpha * q                                                           //
//   rho0 := rho1                                                             //
// UNTIL r is small enough                                                    //
//                                                                            //
// Reference:                                                                 //
// Barrett, R., Berry, M., Chan, T. F., Demmel, J., Donato, J., Dongarra, J., //
//   et al. (1994). Templates for the Solution of Linear Systems: Building    //
//   Blocks for Iterative Methods (2nd ed.). Society for Industrial and       //
//   Applied Mathematics. http://doi.org/10.1137/1.9781611971538              //
////////////////////////////////////////////////////////////////////////////////
void LorascPCG( LorascDenseFunc& func,
                LorascDenseFunc& prec,
                cholmod_dense*& rhs,
                cholmod_dense*& sol,
                cholmod_common* common ) {
  const size_t nrow = func.nrow();
  const size_t nrhs = rhs->ncol;
  int iter = 0;

  // Rename parameters
  auto& Afun = func;
  auto& Mfun = prec;
  auto& b = rhs;
  auto& x = sol;

  // Create arrays
  cholmod_dense* p = nullptr;
  cholmod_dense* q = nullptr;
  cholmod_dense* r = nullptr;
  cholmod_dense* z = nullptr;
  auto rho0 = new double[nrhs];
  auto rho1 = new double[nrhs];
  auto alpha = new double[nrhs];

  // x := 0
  x = cholmod_zeros(
      nrow,          // [in]  #row
      nrhs,          // [in]  #col
      CHOLMOD_REAL,  // [in]  real (double) type
      common         //       CHOLMOD common object
  );

  // r := b - Afun(x)
  r = Afun(x);
  vdSub(
      nrow*nrhs,            // [in]  #entry
      reinterpret_cast
          <double*>(b->x),  // [in]  minuend
      reinterpret_cast
          <double*>(r->x),  // [in]  subtrahend
      reinterpret_cast
          <double*>(r->x)   // [out] difference
  );

  for ( iter = 0; iter < kMaxIter; ++iter ) {
    // z := Mfun(r)
    cholmod_free_dense(&z, common);
    z = Mfun(r);

    // rho1 := r' * z
    for ( int j = 0; j < nrhs; ++j ) {
      rho1[j] = cblas_ddot(
          nrow,                          // [in]  #entry
          reinterpret_cast
              <double*>(r->x) + j*nrow,  // [in]  left vector
          1,                             // [in]  #inc: left vector
          reinterpret_cast
              <double*>(z->x) + j*nrow,  // [in]  right vector
          1                              // [in]  #inc: right vector
      );
    }

    if ( iter == 0 ) {
      // p := z
      p = cholmod_copy_dense(
          z,      // [in]  source
          common  //       CHOLMOD common object
      );
    } else {
      // p := z + (rho1/rho0) * p
      for ( int j = 0; j < nrhs; ++j ) {
        cblas_daxpby(
            nrow,                          // [in]  #entry
            1.0,                           // scalar of summand
            reinterpret_cast
                <double*>(z->x) + j*nrow,  // [in]  summand
            1,                             // [in]  #inc: summand
            rho1[j]/rho0[j],               // scalar of sum
            reinterpret_cast
                <double*>(p->x) + j*nrow,  // [in]  sum
            1                              // [in]  #inc: sum
        );
      }
    }

    // q := Afun(p)
    cholmod_free_dense(&q, common);
    q = Afun(p);

    // alpha := rho1/(p'*q)
    for ( int j = 0; j < nrhs; ++j ) {
      alpha[j] = rho1[j] / cblas_ddot(
          nrow,                          // [in]  #entry
          reinterpret_cast
              <double*>(p->x) + j*nrow,  // [in]  left vector
          1,                             // [in]  #inc: left vector
          reinterpret_cast
              <double*>(q->x) + j*nrow,  // [in]  right vector
          1                              // [in]  #inc: right vector
      );
    }

    // x += alpha * p
    for ( int j = 0; j < nrhs; ++j ) {
      cblas_daxpy(
          nrow,                          // [in]  #entry
          alpha[j],                      // scalar of summand
          reinterpret_cast
              <double*>(p->x) + j*nrow,  // [in]  summand
          1,                             // [in]  #inc: summand
          reinterpret_cast
              <double*>(x->x) + j*nrow,  // [in]  sum
          1                              // [in]  #inc: sum
      );
    }

    // r -= alpha * q
    for ( int j = 0; j < nrhs; ++j ) {
      cblas_daxpy(
          nrow,                          // [in]  #entry
          -alpha[j],                     // scalar of summand
          reinterpret_cast
              <double*>(q->x) + j*nrow,  // [in]  summand
          1,                             // [in]  #inc: summand
          reinterpret_cast
              <double*>(r->x) + j*nrow,  // [in]  sum
          1                              // [in]  #inc: sum
      );
    }

    // rho0 := rho1
    std::swap(rho0, rho1);
  }

  auto err = cblas_dnrm2(
      nrow*nrhs,          // [in]  #entry
      reinterpret_cast
        <double*>(r->x),  // [in]  array
      1                   // [in]  #inc: array
  ) / cblas_dnrm2(
      nrow*nrhs,          // [in]  #entry
      reinterpret_cast
        <double*>(b->x),  // [in]  array
      1                   // [in]  #inc: array
  );
  std::cout << "\rPCG stop with relative residual " << err
            << " in " << iter << " iterations.\n";

  // Delete arrays
  cholmod_free_dense(&z, common);
  cholmod_free_dense(&p, common);
  cholmod_free_dense(&q, common);
  cholmod_free_dense(&r, common);
  delete[] rho0;
  delete[] rho1;
  delete[] alpha;
}

}
