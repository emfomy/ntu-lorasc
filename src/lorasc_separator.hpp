////////////////////////////////////////////////////////////////////////////////
// Robust Algebraic Schur Complement Preconditioners                          //
//     Based on Low Rank Corrections                                          //
//                                                                            //
// lorasc_separator.hpp                                                       //
// The header file of the struct LorascSeparator                              //
//                                                                            //
// Author: emfo<emfomy@gmail.com>                                             //
////////////////////////////////////////////////////////////////////////////////

#ifndef LORASC_LORASC_SEPARATOR_HPP_

#define LORASC_LORASC_SEPARATOR_HPP_

#include "cholmod.h"
#include "lorasc_const.hpp"
#include "lorasc_solver.hpp"
#include "lorasc_auxiliary.hpp"

////////////////////////////////////////////////////////////////////////////////
// The namespace lorasc                                                       //
////////////////////////////////////////////////////////////////////////////////
namespace lorasc {

////////////////////////////////////////////////////////////////////////////////
// The struct of a LORASC separator                                           //
////////////////////////////////////////////////////////////////////////////////
struct LorascSolver::LorascSeparator {
  class SchurFunc;
  class Schur2ConjFunc;
  class SchurPrecFunc;

  // the separator matrix (A_ss)
  // sparse symmmetric positive definite
  cholmod_sparse* matrix_;

  // the factor of the separator matrix (L_ss)
  // sparse lower triangular
  cholmod_factor* factor_;

  // the Schur complement
  // sparse symmmetric positive definite
  cholmod_sparse* schur_;

  // the second term of the Schur complement
  // sparse symmmetric positive definite
  cholmod_sparse* schur2_;

  // the eigenvectors of the Schur complement
  cholmod_dense* eigvec_;

  // the corrected eigenvalues of the Schur complement
  cholmod_sparse* eigval_;

  // the separator right-hand-side vecters
  cholmod_dense* vec_b_;
  cholmod_dense* vec_z_;
  cholmod_dense* vec_y_;
  cholmod_dense* vec_x_;

  // the function handle of the Schur complement
  SchurFunc* schur_func_;

  // the function handle of the Schur2 conjugate
  Schur2ConjFunc* schur2_conj_func_;

  // the function handle of the Schur preconditioner
  SchurPrecFunc* schur_prec_func_;

  // the number of rows of the interior matrix
  size_t nrow_;

  // the permutation array
  int* perm_;

  // the CHOLMOD common object
  cholmod_common* common_;

  // the manual constructor
  void Construct( cholmod_common* common );

  // the manual destructor
  void Destruct();
};

////////////////////////////////////////////////////////////////////////////////
// The class of the function handle of the Schur complement                   //
////////////////////////////////////////////////////////////////////////////////
class LorascSolver::LorascSeparator::SchurFunc: public LorascDenseFunc {
 private:
  // the Schur complement
  // sparse symmmetric positive definite
  cholmod_sparse* schur_;

  // the CHOLMOD common object
  cholmod_common* common_;

 public:
  SchurFunc( cholmod_sparse* matrix,
             cholmod_sparse* schur2,
             cholmod_common* common );
  ~SchurFunc();

  cholmod_dense* operator()( cholmod_dense* src );
};

////////////////////////////////////////////////////////////////////////////////
// The class of the function handle of the Schur2 conjugate                   //
////////////////////////////////////////////////////////////////////////////////
class LorascSolver::LorascSeparator::Schur2ConjFunc: public LorascDenseFunc {
 private:
  // the factor of the separator matrix (L_ss)
  // sparse lower triangular
  cholmod_factor* factor_;

  // the second term of the Schur complement
  // sparse symmmetric positive definite
  cholmod_sparse* schur2_;

  // the CHOLMOD common object
  cholmod_common* common_;

 public:
  Schur2ConjFunc( cholmod_factor* factor,
                  cholmod_sparse* schur2,
                  cholmod_common* common );

  cholmod_dense* operator()( cholmod_dense* src );
};

////////////////////////////////////////////////////////////////////////////////
// The class of the function handle of the Schur preconditioner               //
////////////////////////////////////////////////////////////////////////////////
class LorascSolver::LorascSeparator::SchurPrecFunc: public LorascDenseFunc {
 private:
  // the factor of the separator matrix (L_ss)
  // sparse lower triangular
  cholmod_factor* factor_;

  // the eigenvectors of the Schur complement
  cholmod_dense* eigvec_;

  // the corrected eigenvalues of the Schur complement
  cholmod_sparse* eigval_;

  // the number of the eigenvalues
  size_t rank_;

  // the CHOLMOD common object
  cholmod_common* common_;

 public:
  SchurPrecFunc( cholmod_factor* factor,
                 cholmod_dense* eigvec_,
                 cholmod_sparse* eigval_,
                 size_t rank,
                 cholmod_common* common );

  cholmod_dense* operator()( cholmod_dense* src );
};

}

#endif
