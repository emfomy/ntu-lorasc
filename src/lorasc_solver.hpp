////////////////////////////////////////////////////////////////////////////////
// Robust Algebraic Schur Complement Preconditioners                          //
//     Based on Low Rank Corrections                                          //
//                                                                            //
// lorasc_solver.hpp                                                          //
// The header file of the class LorascSolver                                  //
//                                                                            //
// Author: emfo<emfomy@gmail.com>                                             //
////////////////////////////////////////////////////////////////////////////////

#ifndef LORASC_LORASC_SOLVER_HPP_

#define LORASC_LORASC_SOLVER_HPP_

#include "cholmod.h"
#include "lorasc_const.hpp"

////////////////////////////////////////////////////////////////////////////////
// The namespace lorasc                                                       //
////////////////////////////////////////////////////////////////////////////////
namespace lorasc {

////////////////////////////////////////////////////////////////////////////////
// The class of a LORASC solver                                               //
////////////////////////////////////////////////////////////////////////////////
class LorascSolver {
 private:
  class LorascDomain;
  class LorascSeparator;

  // the domain array
  LorascDomain* domain_;

  // the separator
  LorascSeparator* separator_;

  // the number of levels
  size_t num_level_;

  // the number of domains
  size_t num_domain_;

  // the number of rows of the matrix
  size_t nrow_;

  // the CHOLMOD common object
  cholmod_common* common_;

 public:
  // the constructor
  LorascSolver( const size_t num_level, cholmod_common* common );

  // the destructor
  ~LorascSolver();

  // read a matrix
  void Read( cholmod_sparse* matrix );

  // solve linear system
  void Solve( cholmod_dense* vector );
};

}

#endif
